import java.util.*;
import java.nio.*;

public class ListPractice{
    public static void main(String[] args){
        //Part 1 testing
        String[] strings = {"FIFTY", "12", "Nine", "TWENTYTWO", "6.FIVE"};

        System.out.println("This array contains " + countUpperCase(strings) + " strings with only uppercase letters.");

        String[] result = getUpperCase(strings);
        String print = "These are the words that contain only uppercase letters: ";
        for (int i = 0; i < result.length; i++){
            print += result[i];
            if (i != result.length - 1){
                print += ", ";
            }
        }
        System.out.println(print);

        //Part 2 testing
        List<String> words = new ArrayList<String>();
        System.out.println(words.size());

        for (String word: strings){
            words.add(word);
        }

        System.out.println(words.size());

        System.out.println(countUpperCase(words));

        List<String> result2 = getUpperCase(words);
        String print2 = "These are the words that contain only uppercase letters: ";
        for (int i = 0; i < result2.size(); i++){
            print2 += result2.get(i);
            if (i != result2.size() - 1){
                print2 += ", ";
            }
        }
        System.out.println(print2);
    }

    public static int countUpperCase(String[] strings){
        int count = 0;

        for (int i = 0; i < strings.length; i++){
            if (strings[i].matches("[A-Z]+") == true){
                count++;
            }
        }

        return count;
    }

    public static int countUpperCase(List<String> words){
        int count = 0;

        for (int i = 0; i < words.size(); i++){
            if (words.get(i).matches("[A-Z]+") == true){
                count++;
            }
        }

        return count;
    }

    public static String[] getUpperCase(String[] strings){
        int count = 0;

        for (int i = 0; i < strings.length; i++){
            if (strings[i].matches("[A-Z]+") == true){
                count++;
            }
        }

        String[] result = new String[count];
        int destination = 0;

        for (int i = 0; i < strings.length; i++){
            if (strings[i].matches("[A-Z]+") == true){
                result[destination] = strings[i];
                destination++;
            }
        }

        return result;
    }

    public static List<String> getUpperCase(List<String> words){
        List<String> result = new ArrayList<String>();
        for (String word: words){
            if (word.matches("[A-Z]+") == true){
                result.add(word);
            }
        }

        return result;
    }

    
}